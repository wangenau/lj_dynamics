"""Trajectory file viewer utilities for Jupyter notebooks."""

import pathlib
import uuid

import numpy as np
from nglview import NGLWidget
from nglview.base_adaptor import Structure, Trajectory


def rad2deg(a):
    """Convert Radians to Degree."""
    return a * 180 / np.pi


def vector_angle(a, b):
    """Calculate the angle between two vectors."""
    a_norm = a / np.linalg.norm(a)
    b_norm = b / np.linalg.norm(b)
    angle = np.arccos(a_norm @ b_norm)
    return rad2deg(angle)


def create_pdb_str(atom, pos, box=None):
    """Convert atom symbols and positions to the PDB format."""
    pdb = ""
    if box is not None:
        pdb += "CRYST1"  # 1-6 "CRYST1"
        pdb += f"{np.linalg.norm(box[0]):>9,.3f}"  # 7-15 a
        pdb += f"{np.linalg.norm(box[1]):>9,.3f}"  # 16-24 b
        pdb += f"{np.linalg.norm(box[2]):>9,.3f}"  # 25-33 c
        pdb += f"{vector_angle(box[1], box[2]):>7,.2f}"  # 34-40 alpha
        pdb += f"{vector_angle(box[0], box[2]):>7,.2f}"  # 41-47 beta
        pdb += f"{vector_angle(box[0], box[1]):>7,.2f}"  # 48-54 gamma
        pdb += " "
        pdb += "P 1        "  # 56-66 Space group
        pdb += "\n"

    pdb += "MODEL 1"
    for ia in range(len(atom)):
        pdb += "\nATOM  "  # 1-6 "ATOM"
        pdb += f"{ia + 1:>5}"  # 7-11 Atom serial number
        pdb += " "
        pdb += f"{atom[ia]:>4}"  # 13-16 Atom name
        pdb += " "  # 17 Alternate location indicator
        pdb += "MOL"  # 18-20 Residue name
        pdb += " "
        pdb += " "  # 22 Chain identifier
        pdb += "   1"  # 23-26 Residue sequence number
        pdb += " "  # 27 Code for insertions of residues
        pdb += "   "
        pdb += f"{pos[ia, 0]:>8,.3f}"  # 31-38 X orthogonal coordinate
        pdb += f"{pos[ia, 1]:>8,.3f}"  # 39-46 Y orthogonal coordinate
        pdb += f"{pos[ia, 2]:>8,.3f}"  # 47-54 Z orthogonal coordinate
        pdb += f"{1:>6,.2f}"  # 55-60 Occupancy
        pdb += f"{0:>6,.2f}"  # 61-66 Temperature factor
        pdb += "          "
        pdb += f"{atom[ia]:>2}"  # 77-78 Element symbol
    return f"{pdb}\nENDMDL\n"


def read_traj(filename):
    """Load atom species and positions from TRAJ files."""
    with pathlib.Path(filename).open(encoding="utf-8") as fh:
        lines = fh.readlines()
        n_lines = len(lines)
        n_atoms = int(lines[0].strip())
        traj = []
        for frame in range(n_lines // (2 + n_atoms)):
            atom = []
            pos = []
            for line in lines[(2 + n_atoms) * frame + 2 : (2 + n_atoms) * (frame + 1)]:
                line_split = line.strip().split()
                atom.append(line_split[0])
                pos.append(np.float64(line_split[1:4]))
            pos = np.asarray(pos)
            traj.append((atom, pos))
    return traj


class MDTrajectory(Trajectory, Structure):
    """MDTrajectory class to handle trajectory files."""

    def __init__(self, filename, box=None):
        """Initialize the MDTrajectory object."""
        self.trajectory = read_traj(filename)
        if box is not None and box.ndim == 1:
            box = box * np.eye(3)
        self.box = box
        self.ext = "pdb"
        self.params = {}
        self.id = str(uuid.uuid4())

    def get_coordinates(self, index):
        """Get the atom coordinates for a given frame."""
        return self.trajectory[index][1]

    @property
    def n_frames(self):
        """Number of frames."""
        return len(self.trajectory)

    def get_structure_string(self, index=0):
        """Get the structure string per frame in the PDB format."""
        return create_pdb_str(
            self.trajectory[index][0],
            self.trajectory[index][1],
            self.box,
        )


def view_traj(filename, box=None):
    """Display molecules for a given TRAJ file."""
    view = NGLWidget()
    trajectory = MDTrajectory(filename, box)
    view.add_trajectory(trajectory)
    if box is not None:
        view.add_unitcell()
    return view
