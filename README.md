# Lennard-Jones Molecular Dynamics

Learn something about molecular dynamics. This repository implements 3D PBC molecular dynamics for argon atoms to investigate the formation of Lennard-Jones clusters.

Find a render of the notebook [here](https://wangenau.gitlab.io/lj_dynamics).

<img src="lj_dynamics/lj_md.gif
" alt="drawing" width="450"/>

## Requirements
Jupyter with a Python kernel is required. For package dependencies see the [requirements.txt](requirements.txt) file for details. To install them, use:
```
pip3 install -r requirements.txt
```

The visualization uses [nglview](https://github.com/nglviewer/nglview). It does not work in VSCode.

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
